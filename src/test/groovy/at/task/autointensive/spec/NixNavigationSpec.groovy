package at.task.autointensive.spec

import geb.spock.GebReportingSpec
import at.task.autointensive.page.BlogPage
import at.task.autointensive.page.StartPage


class NixNavigationSpec extends GebReportingSpec {
    def "Navigate to Blog page" (){
        when:
        to StartPage
        and:
        "User navigates to Blog Page"()
        then:
        at BlogPage
    }
}
